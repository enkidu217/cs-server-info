module Main where

import Network.Socket hiding (recv)
import Network.Socket.ByteString (sendAll, recv)
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as B
import QueryServer

main :: IO()
main = withSocketsDo $
     do addrinfos <- getAddrInfo Nothing (Just "74.91.123.66") (Just "27015")
        let serveraddr = head addrinfos
        sock <- socket (addrFamily serveraddr) Datagram defaultProtocol
        connect sock (addrAddress serveraddr)
        chal <- getChallenge sock
        putStr "Received Challenge\n"
        putStrLn $ show (B.unpack chal)
        players <- getPlayers sock chal
        putStrLn $ show (B.unpack $ players)
        C.putStrLn players
        sClose sock
        


