module QueryServer (getChallenge, getPlayers, getInfo, getRules) where

 import Network.Socket hiding (recv)
 import Network.Socket.ByteString (recv, sendMany)
 import qualified Data.ByteString.Char8 as C

 header::C.ByteString
 header = C.pack "\255\255\255\255"
 
 receive :: Socket -> String -> C.ByteString -> IO C.ByteString
 receive sock h p = withSocketsDo $
                   do sendMany sock [header, C.pack h, p]
                      msg <- recv sock 4096
                      return $ C.drop 5 $ msg


 -- hopefully ends up being
 --getInfo :: Socket -> Either Error Info
 getInfo :: Socket -> IO C.ByteString
 getInfo sock =
   do
     msg <- receive sock "T" (C.pack "Source Engine Query")
     -- do some parsing/error checking
     return msg

 getChallenge :: Socket -> IO C.ByteString
 getChallenge sock =
   do
     msg <- receive sock "U" (C.pack "\255\255\255\255")
     return msg
        
 getPlayers :: Socket -> C.ByteString -> IO C.ByteString
 getPlayers sock chal =
   do
     msg <- receive sock "U" chal
     return  msg

 getRules :: Socket -> C.ByteString -> IO C.ByteString
 getRules sock chal =
   do
     msg <- receive sock "V" chal
     return msg
